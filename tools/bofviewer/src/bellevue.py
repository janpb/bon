#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  \file bellevue.py
#  \copyright 2016-2018 The University of Sydney
#  \author Jan P Buchmann <lejosh@members.fsf.org>
#  \author Mathieu Fourment <mathieu.fourment@uts.edu.au>
#  \description A simple viewer for bon files. Can read files but uses standard
#               input by default.
#-------------------------------------------------------------------------------
import sys
import os
import json
import argparse

sys.path.insert(1, os.path.join(sys.path[0], '../../../src'))
import bon
import reader

class BonViewer(bon.Bon):

  ## Constructor
  # Initalizes BonViewer() by inheriting Bon()
  # Sets the default parameters
  def __init__(self):
    super().__init__()
    self.sep = ':'
    self.decomp = True
    self.aoi = []

  ## Method to adjust parameters from arguments
  #@params, args, argparse argument instance
  def setup(self, args):
    if len(args.attributes) > 0:
      self.aoi = args.attributes
    self.sep = args.separator
    self.decomp = args.no_decomp

  ## Method to dump attributes to standard output
  def dump_attribute(self, attribute_name, attribute_data):
    print(self.sep.join([str(attribute_name), str(attribute_data)]))

  ## Implementation of virtual  parse method from Bon()
  #@params obj, str, JSON object
  def parse(self, obj):
    data = json.loads(obj)
    if len(self.aoi) > 0:
      for i in self.aoi:
        if self.isCompressed(i) and self.decomp:
          self.dump_attribute(i, self.expand_data(data[i]))
        else:
          self.dump_attribute(i, data[i])
    else:
      for i in data:
        if self.isCompressed(i) and self.decomp:
          self.dump_attribute(i, self.expand_data(data[i]))
        else:
          self.dump_attribute(i, data[i])

def main():
  ap = argparse.ArgumentParser(description='Bellevue, a simple Bon block      \
                                viewer.  Reads bon blocks from standard input \
                                or file')
  ap.add_argument('-a', '--attributes', nargs='*', default=[],
                  help='Attributes to parse. Default: Show all')
  ap.add_argument('-n', '--no_decomp', action='store_false',
                  help='No decompression of compressed data.')
  ap.add_argument('-s', '--separator', type=str, default=':',
                  help='Set separator for attribute and value during print')
  ap.add_argument('--header', action='store_true',
                  help='Print header')

  args = ap.parse_args()
  v = BonViewer()
  v.setup(args)
  r = reader.BonReader()
  r.read(v)
  return 0

if __name__ == '__main__':
  main()
