#-------------------------------------------------------------------------------
#  Copyright 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  newick.py
#  Authors:
#   Mathieu Fourment <mathieu.fourment@uts.edu.au>
#   Jan P Buchmann   <jan.buchmann@sydney.edu.au>
#  Description:
#   Convert phylogenetic trees in Newick annotation into bof
#-------------------------------------------------------------------------------
import io
import sys
import json
from Bio import Phylo
from Bio.Phylo import BaseTree
from . import bof

class PhylogeneticTree(bof.BofObject):

  def __init__(self, header, compressor):
    super().__init__(header, compressor)


class NewickConverter(bof.Bof):

  def __init__(self, compress):
    super().__init__(compress=compress)

  def convert(self):
    nwtree = ''
    tree_idx = 0
    while True:
      char = sys.stdin.read(1)
      if len(char) == 0:
        break
      if char == '\n':
        continue
      elif char == ';':
        tree = Phylo.read(io.StringIO(nwtree), "newick")
        t = PhylogeneticTree(self.header, self.compressor)
        if self.header.compression == True:
          t.add_attribute('tree', json.dumps(self.dictionize(tree.root)))
        else:
          t.add_attribute('tree', self.dictionize(tree.root))
#        t.add_attribute('leafnames', {x.name:0 for x in tree.get_terminals()})
        t.add_attribute('leaf_nodes', tree.count_terminals())
        t.add_attribute('internal_nodes', len(tree.get_nonterminals()))
        t.add_attribute('nodes', tree.count_terminals() + len(tree.get_nonterminals()))
        t.add_attribute('rooted', tree.rooted)
        t.add_attribute('weight', tree.weight)
        t.add_attribute('name', "tree_"+str(tree_idx))
        tree_idx += 1
        self.add(t)
        nwtree = ''
      else:
        nwtree += char

  def dictionize(self, node):
      """
      Convert a Tree object to a dictionary using postorder traversal
      :param node: current node
      :return: node as a dict
      """
      d = dict()
      if node.name:
          d['name'] = node.name
      if node.branch_length:
          d['branch_length'] = node.branch_length

      if not node.is_terminal():
          d['clade'] = [self.dictionize(child) for child in node]
      return d


  def dict2tree(self, subdic):
      """
      Convert a dictionary to a tree
      :param subdic: current node represented by a dict
      :return: current node object
      """
      clade = BaseTree.Clade(branch_length=subdic.get('branch_length'), name=subdic.get('name'))
      if 'clade' in subdic:
          clade.clades = [dict2tree(x) for x in subdic['clade']]
      return clade
