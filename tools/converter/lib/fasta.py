#  Copyright 2016 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  fasta.py
#  Author: Jan P Buchmann <lejosh@members.fsf.org>
#  Description:
#   Convert FASTA into bof

import sys
sys.path.insert(1, os.path.join(sys.path[0], '../../src'))
import bof

class FastaSequence(bof.BofObject):

  def __init__(self, header, compressor):
    super().__init__(header, compressor)

class FastaConverter(bof.Bof):

  def __init__(self, compress):
    super().__init__(compress=compress)

  def convert(self):
    sequence = ''
    header = ''
    for i in sys.stdin:
      if i[0] == ">":
        if len(sequence) > 0:
          self.add_seq(header, sequence)
        header = i[1:].strip()
        sequence = ''
        continue
      sequence += i.strip()
    self.add_seq(header, sequence)

  def add_seq(self, header, seq):
    s = FastaSequence()
    s.add_attribute('sequence', seq)
    s.add_attribute('header', header)
    s.add_attribute('length', len(seq))
    self.add(s)
