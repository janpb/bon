#-------------------------------------------------------------------------------
#  Copyright 2016, 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  \file bioxml.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \author Mathieu Fourment <mathieu.fourment@uts.edu.au>
#  \description NCBI TinySeq XML to bon converter.
#  \version 1.1.0
#-------------------------------------------------------------------------------
import os
import sys
import xml.etree.ElementTree as ET

sys.path.insert(1, os.path.join(sys.path[0], '../../src'))
import bon
import bon_object

## Bon object for TinySeq sequences.
class TinySeqSequence(bon_object.BonObject):

  ##Constructor
  # Initalizes a new TinySeq Bon object. The tagmap simplifies to parse
  # the XML tags
  def __init__(self):
    super().__init__()
    self.tagmap = {
                   'TSeq_gi' : 'gi',
                   'TSeq_sid' : 'id',
                   'TSeq_taxid' : 'taxid',
                   'TSeq_accver' : 'accver',
                   'TSeq_length' : 'length',
                   'TSeq_seqtype': 'typ',
                   'TSeq_defline' : 'defline',
                   'TSeq_orgname' : 'orgname',
                   'TSeq_sequence' : 'sequence'
                  }

## NCBI TinySeq XML to Bon converter
# Inherits Bon() and implements its own convert method
class TinySeqConverter(bon.Bon):

  ## Constructor
  # Initalize default settings and data attributes
  #@param, attributes, dictionary, data attribute  dictionray required by Bon()
  def __init__(self, attributes):
    super().__init__(data=attributes)
    self.src = sys.stdin
    self.isStream = True

  ## Implementation of virtual convert function from Bon()
  def convert(self, fil=None, s=TinySeqSequence()):
    if fil != None:
      self.src = open(fil, 'r')
      self.isStream = False
    for event, elem in ET.iterparse(self.src, events=["start", "end"]):
      if event == 'start' and elem.tag == 'TSeq':
        s = TinySeqSequence()
      if event == 'end' and elem.tag == 'TSeq':
        for i in s.tagmap:
          s.add_attribute(s.tagmap[i], 0)
        s.tagmap = {}
        self.add(s)
        if self.header.size % 10 == 0:
          print("\r\tXmlConverter::converted {0} entries".format(self.header.size),
                end='', file=sys.stderr)
      if event == 'end' and elem.tag in s.tagmap:
        if elem.tag == 'TSeq_seqtype':
          s.add_attribute(s.tagmap[elem.tag], elem.attrib['value'])
        else:
          s.add_attribute(s.tagmap[elem.tag], elem.text)
        s.tagmap.pop(elem.tag)

    if self.isStream == False:
      self.src.close()
