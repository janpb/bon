#-------------------------------------------------------------------------------
#  Copyright 2016, 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  \file fastq.py
#  \author Jan P Buchmann <lejosh@members.fsf.org>
#  \author Mathieu Fourment <mathieu.fourment@uts.edu.au>
#  \description Simple FASTQ to bon converter. Reading FASTQ files assume no
#               line breaks in sequence and quality. The converter would work
#               with a plain Bon() object, but it serves as an example how to
#               use Bon().
#-------------------------------------------------------------------------------
import os
import sys

sys.path.insert(1, os.path.join(sys.path[0], '../../src'))
import bon
import bon_object

## Bon object for FASTQ entries.
class FastqSequence(bon_object.BonObject):

  ##Constructor
  # Initalizes a new FASTQ  Bon() object.
  def __init__(self):
    super().__init__()

## FASTQ to Bon converter
# Inherits Bon() and implements its own convert method
class FastqConverter(bon.Bon):

  def __init__(self, attributes):
    super().__init__(data=attributes)
    self.src = sys.stdin
    self.isStream = True

  ## Implementation of virtual convert function from Bon()
  # @param fil, string, path to FATSQ file, STDIN if None
  # @param s, Initializes an empty FastqSequence()
  def convert(self, fil=None, s=FastqSequence()):
    if fil != None:
      self.src = open(fil, 'r')
      self.isStream = False
    lcount  = 0
    for i in self.src:
      lcount += 1
      if lcount == 1:
        s = FastqSequence()
        s.add_attribute('spotid', i.rstrip()[1:].split()[0])
        s.add_attribute('readid', i.rstrip().split()[1])
      if lcount == 2:
        s.add_attribute('length',  len(i.strip()))
        s.add_attribute('sequence', i.strip())
      if lcount == 4:
        s.add_attribute('quality', i.strip())
        self.add(s)
        if self.header.size % 10000 == 0:
          print("\r\tFastqConverter::converted {0} entries".format(self.header.size), end='',
            file=sys.stderr)
        lcount = 0
    if self.isStream == False:
      self.src.close()
