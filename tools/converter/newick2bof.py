#!/usr/bin/python3
# -*- coding: utf-8 -*-
#  Copyright 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  newick2bof.py
#  Authors: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#           Mathieu Fourment <mathieu.fourment@uts.edu.au>
#  Description:
#   A tool to convert a Newick phylogeny into a  bof.
#  Version: 0

import os
import sys
import argparse
import lib.newick

def main():
  ap = argparse.ArgumentParser(description='Newick to bof (from STDIN)')
  ap.add_argument('-c', '--compress', action='store_true',
                  help = 'Compress sequences')
  args = ap.parse_args()

  compress={}
  if args.compress == True:
    compress={'tree' : 1}

  c = lib.newick.NewickConverter(compress)
  c.convert()
  for i in c.write():
    print(i, end='')

  return 0

if __name__ == '__main__':
  main()
