#!/usr/bin/python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  Copyright 2016 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  \file xml2bof.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \author Mathieu Fourment <mathieu.fourment@uts.edu.au>
#  \description Frontend for NCBI TinySeq XML to bon converter.
#  \version 1.1.0
#-------------------------------------------------------------------------------
import os
import argparse
import sys
import lib.bioxml


def main():
  ap = argparse.ArgumentParser(description='NCBI TinySeq XML to bon converter.\
                                            Reads data from standard inoput')
  ap.add_argument('-c', '--compress', action='append', default=[],
                  help='Compress attribute. Use multiple times, e.g.\
                        -c attributeA -c AttributeB...')
  ap.add_argument('-a', '--show_attributes', action='store_true',
                  help = 'Show possible FASTQ attributes'),
  ap.add_argument('-p', '--pretty', action='store_true',
                  help = 'Enable pretty print')
  ap.add_argument('-d', '--dump', action='store_true',
                  help = 'Dump entries')
  args = ap.parse_args()

  attributes={'defline' : 0, 'typ' : 0, 'gi' : 0, 'taxid' : 0, 'length' : 0,
              'orgname' : 0, 'sequence': 0, 'accver' : 0,'id' : 0}

  if args.show_attributes:
    print("Possible bon attributes for NCBI TinySeq files:")
    for i in attributes:
      print("\t",i)
    sys.exit()

  if len(args.compress) > 0:
    for i in args.compress:
      if i in attributes:
        attributes[i] = 1
  c = lib.bioxml.TinySeqConverter(attributes)
  c.convert()
  if args.dump == True:
    for i in c.write():
      print(i, end='')
  else:
    c.export(args.pretty)
  return 0

if __name__ == '__main__':
  main()
