#!/usr/bin/python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# \file nexus2bof.py
# \copyright 2017, 2018 The University of Sydney
# \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
# \author  Mathieu Fourment <mathieu.fourment@uts.edu.au>
# \description Frontend for Nexus to bon converter via NeXML
#-------------------------------------------------------------------------------
import sys
import argparse
import lib.phylo

def main():
  ap = argparse.ArgumentParser(description='Nexus to bon converter. Nexus files\
                                            are read via standard input')
  ap.add_argument('-c', '--compress', action='append',default=[],
                  help='Compress attribute. Use multiple times, e.g.\
                        -c attributeA -c AttributeB...')
  ap.add_argument('-a', '--show_attributes', action='store_true',
                  help = 'Show possible FASTQ attributes'),
  ap.add_argument('-i', '--input', type=argparse.FileType('r', encoding='UTF-8'),
                  help = 'Nexus input file sequences')
  args = ap.parse_args()

  attributes={'tree'           : 0,
              'leaf_nodes'     : 0,
              'internal_nodes' : 0,
              'nodes'          : 0,
              'rooted'         : 0,
              'name'           : 0}

  if args.show_attributes:
    print("Possible bof attributes for NEXUS files:")
    for i in attributes:
      print("\t",i)
    sys.exit()

  if len(args.compress) > 0:
    for i in args.compress:
      if i in attributes:
        attributes[i] = 1

  c = lib.phylo.PhyloTreeConverter(attributes)
  c.convert(args.input)
  for i in c.write():
    print(i, end='')

  return 0

if __name__ == '__main__':
  main()
