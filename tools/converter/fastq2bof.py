#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  Copyright 2016, 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  \file fastq2bof.py
#  \author Jan P Buchmann <lejosh@members.fsf.org>
#  \author  Mathieu Fourment <mathieu.fourment@uts.edu.au>
#  \description Frontend to convert FASTQ files into bon.
#-------------------------------------------------------------------------------
import os
import sys
import argparse

import lib.fastq

def main():
  ap = argparse.ArgumentParser(description='FASTQ to bon converter. Reads \
                                            FASTQ files from standard input')
  ap.add_argument('-c', '--compress', action='append',default=[],
                  help='Compress attribute. Use multiple times, e.g. \
                        -c attributeA -c AttributeB...')
  ap.add_argument('-a', '--show_attributes', action='store_true',
                  help = 'Show possible FASTQ attributes'),
  ap.add_argument('-p', '--pretty', action='store_true',
                  help = 'Enable pretty print for bof (larger file size)'),
  ap.add_argument('-d', '--dump', action='store_true',
                  help = 'Dump entries')
  args = ap.parse_args()
  attributes={
            'spotid'   : 0,
            'readid'   : 0,
            'length'   : 0,
            'sequence' : 0,
            'quality'  : 0
            }
  if args.show_attributes:
    print("Possible bof attributes for FASTQ files:")
    for i in attributes:
      print("\t",i)
    sys.exit()

  if len(args.compress) > 0:
    for i in args.compress:
      if i in attributes:
        attributes[i] = 1
  c = lib.fastq.FastqConverter(attributes)
  c.convert()
  if args.dump == True:
    for i in c.write():
      print(i, end='')
  else:
    c.export(args.pretty)
  return 0

if __name__ == '__main__':
  main()
