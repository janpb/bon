#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  figure.py
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#
#  Version: 0
import matplotlib
import numpy
from . import plotter

class Figure:

  def __init__(self):
    self.bgcolor = 'white'
    self.fgcolor = '#262626'
    self.ticklabelsize = 17
    self.labelsize = 19
    self.tickwidth = 0
    self.labelmap = {}
    self.tickpad = 0

  def set_bg(self, ax):
    ax.yaxis.label.set_size(self.labelsize)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.set_facecolor(self.bgcolor)

  def set_yticks(self, ax):
    for i in ax.yaxis.get_majorticklocs()[1:]:
      ax.axhline(y=i, linestyle=':', c=self.bgcolor, lw=0.5, alpha=1)

  def set_basic_design(self, ax):
    ax.tick_params(axis='both',
                   direction='out',
                   colors=self.fgcolor,
                   labelsize=self.ticklabelsize,
                   width=self.tickwidth,
                   pad=self.tickpad,
                   which='major',
                   top=False,
                   labeltop=False,
                   bottom=False,
                   labelbottom=False,
                   labelright=False,
                   right=False,
                   left=False,
                   labelleft=False)
    self.set_bg(ax)
    self.set_yticks(ax)

  def design_fsize_plot(self, ax):
    self.set_basic_design(ax)
    ax.tick_params(axis='both',  labelleft=True)

  def design_seqsize_plot(self, ax):
    self.set_basic_design(ax)
    ax.tick_params(axis='both', labelbottom=True, labelleft=True)

  def plot(self, plotgroups):
    for i in plotgroups:
        fig = matplotlib.pyplot.figure(figsize=(12, 9))
        ax0 = fig.add_subplot(2, 1, 1)
        fsp = plotter.FileSizePlotter()
        fsp.plot(ax0, plotgroups[i], [x[1] for x in plotgroups[i].plotorder])
        self.design_fsize_plot(ax0)

        ssp = plotter.SeqSizePlotter()
        ssp.xticks_pos = fsp.xticks_pos
        ax1 = fig.add_subplot(2,1,2, sharex=ax0)
        ssp.plot(ax1, plotgroups[i], [x[1] for x in plotgroups[i].plotorder])
        self.design_seqsize_plot(ax1)
        fig.legend(fsp.legend.artists, fsp.legend.labels, fontsize='large', frameon=False)
        fig.savefig(i+'.bof.plot.svg', bbox_inches='tight')
        fig.savefig(i+'.bof.plot.pdf', bbox_inches='tight')
