#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  ds_plotter.py
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#
#  Version: 0

import sys
import math
import sqlite3
import argparse
import database.database
import plotter.figure
import analyzer

class Plotgroup:

  class  Plot:

    def __init__(self, name, size, szDataDecomp, szDataComp):
      self.name = name
      self.size = size
      self.szDataDecomp = szDataDecomp
      self.szDataComp = szDataComp
      self.fsizes = {}
      self.barwidth = 0
      self.width = 1
      self.samples = []
      self.max_fsize = 0
      self.max_sample = 0

    def set_max_fsize(self):
      self.max_fsize = max(self.fsizes[x][0] for x in self.fsizes)

    def add_fsize(self, fmt, fsize):
      if fmt == 'cbof' or fmt == 'cnexml' or fmt == 'cxml' or fmt == 'cfq':
        self.fsizes[fmt] = [fsize, self.szDataComp]
      else:
        self.fsizes[fmt] = [fsize, self.szDataDecomp]
      self.width = 1 / (len(self.fsizes) + 0.6)
      self.barwidth = self.width - self.width * 0.2

  def __init__(self, name, unit, db):
    self.name = name
    self.sample_unit = unit
    self.fs_order = ['nexml', 'cnexml', 'fq', 'cfq', 'xml', 'cxml', 'bof', 'cbof']
    self.plots = {}
    self.db = database.database.Database()
    self.db.connect(db)
    self.plotorder = []
    self.fsize_units = {
                         10 : 'KiB',
                         20 : 'MiB',
                         30 : 'GiB',
                         40 : 'TiB'
                       }
    self.sample_prefix = {
                          3 : 'K',
                          6 : 'M',
                          9 : 'T'
                        }

    self.sample_unit_factor = 1
    self.fsize_unit = ''
    self.fsize_factor = 1

  def set_plot_order(self):
    for i in self.plots:
      self.plots[i].set_max_fsize()
      self.plotorder.append((self.plots[i].max_fsize, self.plots[i].name))
    self.plotorder.sort(key=lambda x: x[0], reverse=True)
    self.set_fsize_unit()

  def set_fsize_unit(self):
    magnitude = math.floor(math.log2(max(self.plots[x].max_fsize for x in self.plots)))
    self.fsize_factor = 2**(magnitude - magnitude % 10)
    self.fsize_unit = self.fsize_units[magnitude - magnitude % 10]
    sample_magnitude = math.floor(math.log10(max(max(self.plots[x].samples for x in self.plots))))
    self.sample_unit_factor = math.floor(sample_magnitude/3)*3
    if self.sample_unit_factor > 0:
      self.sample_unit = self.sample_prefix[self.sample_unit_factor] + self.sample_unit

  def add_plot(self, db_dsetId, name, size, szDataDecomp, szDataComp):
    if name not in self.plots:
      self.plots[name] = self.Plot(name, size, szDataDecomp, szDataComp)
      self.plots[name].samples = self.add_samples(db_dsetId)

  def add_samples(self, dset_id):
    return [int(i[0]) for i in self.db.execute_sql_cmd("""SELECT s.size, ds.id, ds.name
               FROM samples s JOIN  dsets ds ON s.dsetId=? WHERE ds.id=?""",
              (dset_id, dset_id))]

class DatasetAssembler:

  def __init__(self):
    self.db = database.database.Database()
    self.plotgroups = {}
    self.datasets = {
                      'genomes_zmays'             : ['genome','bp', 'Zmays'],
                      'genomes_dmela'             : ['genome','bp', 'Dmela'],
                      'genomes_bdist'             : ['genome','bp', 'Bdist'],
                      'genomes_hsapi'             : ['genome','bp', 'Hsapi'],
                      'genomes_mmusc'             : ['genome','bp', 'Mmusc'],
                      'protein_hsapi'             : ['protein', 'aa','Hsapi'],
                      'protein_plants'            : ['protein', 'aa','Plants'],
                      'collection_virus_genomes'  : ['collection','bp','Virus genomes'],
                      'collection_plants_est'     : ['collection','bp','Plant EST'],
                      'genomes_bgrami'            : ['collection','bp','Bgram scaffolds'],
                      'ERR1662731'                : ['SRA','bp', 'ERR1662731'],
                      'ERR1988801'                : ['SRA','bp', 'ERR1988801'],
                      'SRR390728'                 : ['SRA','bp', 'SRR390728'],
                      'SRR5710238'                : ['SRA','bp', 'SRR5710238'],
                      'SRR5787977'                : ['SRA','bp', 'SRR5787977'],
                      'SRR5802898'                : ['SRA','bp', 'SRR5802898'],
                      'treebase0'                 : ['phylo','Phylogenetic tree nodes','trees0'],
                      'treebase1'                 : ['phylo','Phylogenetic tree nodes','trees1'],
                      'treebase2'                 : ['phylo','Phylogenetic tree nodes','trees2']
                     }

  def connect(self, db):
    self.db.connect(db)

  def collect_datasets(self):
    for i in self.db.execute_sql_cmd("""SELECT ds.id,
                                               ds.name,
                                               ds.size,
                                               ds.szDataDecomp,
                                               ds.szDataCompBof,
                                               ds.szDataCompZlib,
                                               fs.fmt,
                                               fs.fsize
                                               FROM dsets ds JOIN fsizes fs
                                               ON ds.id=fs.dsetId"""):
      if i[1] in self.datasets:
        if self.datasets[i[1]][0] not in self.plotgroups:
          self.plotgroups[self.datasets[i[1]][0]] = Plotgroup(self.datasets[i[1]][0],
                                                              self.datasets[i[1]][1],
                                                              self.db.db)
        compsize = i[5]
        if i[6] == "bof" or i[6] == "cbof":
          compsize = i[4]
        self.plotgroups[self.datasets[i[1]][0]].add_plot(i[0],
                                                         self.datasets[i[1]][2],
                                                         i[2],
                                                         i[3],
                                                         compsize)
        self.plotgroups[self.datasets[i[1]][0]].plots[self.datasets[i[1]][2]].add_fsize(i[6], i[7])

    for i in self.plotgroups:
      self.plotgroups[i].set_plot_order()

  def plot(self):
    f = plotter.figure.Figure()
    f.plot(self.plotgroups)

def main():
  ap = argparse.ArgumentParser(description='Bof data plotter')
  ap.add_argument('-db', '--database', type=str, help='Path for databases',
                  required=True)
  args = ap.parse_args()

  da = DatasetAssembler()
  da.connect(args.database)
  da.collect_datasets()
  da.plot()
  return 0

if __name__ == '__main__':
  main()
