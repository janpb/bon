#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Copyright 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  \file ds_analyzer.py
#  \author P Buchmann <jan.buchmann@sydney.edu.au>
#  \description Front end to run a data analysis to compare bof files with
#               its equivalents
#  \version 1.0.1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
import argparse
import analyzer.dataset_analyzer
import analyzer.summarizer

class TinySeqAnalyzer(analyzer.dataset_analyzer.TinySeqAnalyzer):

  def __init__(self, args, name):
    super().__init__(name=name)
    self.write = args.write
    self.attributes={
                     'defline' : 0,
                     'typ'     : 0,
                     'gi'      : 0,
                     'taxid'   : 0,
                     'length'  : 0,
                     'orgname' : 0,
                     'sequence': 1,
                     'accver'  : 0,
                     'id'      : 0
                     }
class FastqAnalyzer(analyzer.dataset_analyzer.FastqAnalyzer):

  def __init__(self, args, name):
    super().__init__(name=name)
    self.write = args.write
    self.attributes={
                     'spotid'   : 0,
                     'readid'   : 0,
                     'length'   : 0,
                     'sequence' : 1,
                     'quality'  : 1
                     }
class PhyloTreeAnalyzer(analyzer.dataset_analyzer.PhyloTreeAnalyzer):

  def __init__(self, args, name):
    super().__init__(name=name)
    self.write = args.write
    self.attributes={'tree'           : 1,
                     'leaf_nodes'     : 0,
                     'internal_nodes' : 0,
                     'nodes'          : 0,
                     'rooted'         : 0,
                     'name'           : 0}

def main():
  ap = argparse.ArgumentParser(description='Analyze bof data sets')
  ap.add_argument('-xml', action='store_true', help='analyse xml file'),
  ap.add_argument('-fq',  action='store_true', help='analyse fastq file'),
  ap.add_argument('-tree',  action='store_true', help='analyse phylogenetic tree file'),
  ap.add_argument('--db',  type=str, help='path to sqlite3 database'),
  ap.add_argument('--name', type=str, help='Set dataset name'),
  ap.add_argument('--file', type=str, help='Input file'),
  ap.add_argument('--summarize', action='store_true', help='Summarize exisitng tables. Call last.'),
  ap.add_argument('-w', '--write', action='store_true', help='Write converted files onto disk')
  args = ap.parse_args()

  analyses = []
  if args.xml:
    analyses.append(TinySeqAnalyzer(args, name=args.name))
  if args.fq:
    analyses.append(FastqAnalyzer(args, name=args.name))
  if args.tree:
    analyses.append(PhyloTreeAnalyzer(args, name=args.name))

  if args.summarize:
    s = analyzer.summarizer.DatasetSummarizer(args.db)
    s.summarize()

  for i in analyses:
    i.analyze(args.file)
    i.add_to_db(args.db)
  return 0

if __name__ == '__main__':
  main()
