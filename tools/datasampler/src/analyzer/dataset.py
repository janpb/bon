#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  \file dataset.py
#  \copyright 2017 The University of Sydney
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \version: 1.0.1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import math
import zlib
import base64

class Dataset:
  # longest nulcleotide sequence in NCBI (30.05.2017):
  #   - 774434471 ( root[organism] Sort by: SLEN )
  class Attribute:

      def __init__(self):
        self.shannon    = 0
        self.rawsize    = 0
        self.compsize   = 0
        self.name       = 0
        self.compsize_bof = 0
        self.compsize_zlib = 0

  class Sample:

    def __init__(self, name, id, size):
      self.name = name
      self.id   = id
      self.size = size
      self.attribute_props = {}

  def __init__(self):
    self.src = ''
    self.typ = ''
    self.name = ''
    self.fsizes = {}
    self.samples = []
    self.comp_datasize = 0
    self.raw_datasize = 0

  def add_fmts(self, fmts):
    self.fsizes = {x:0 for x in fmts}

  def add_sample(self, name, id, size, bofobj, converter):
    s = self.Sample(name, id, size)
    self.compression_calculator(s, bofobj, converter, True)
    converter.expand_single(bofobj)
    self.compression_calculator(s, bofobj, converter, False)
    self.samples.append(s)

  def calc_shannon(self, data):
    charmap = {}
    h = 0
    for i in data:
      if i not in charmap:
        charmap[i] = {'count' : 0, 'freq' : 0}
      charmap[i]['count'] += 1
    for i in charmap:
      charmap[i]['freq'] = charmap[i]['count']/len(data)
      h += charmap[i]['freq'] * math.log2(charmap[i]['freq'])
    return h*-1

  def compression_calculator(self, sample, bofobj, converter, compressed):
    for i in bofobj.data:
      if i not in sample.attribute_props:
        sample.attribute_props[i] = self.Attribute()
      sample.attribute_props[i].name = i
      if i in converter.header.data:
        if converter.header.data[i] == 1 and compressed == True:
          sample.attribute_props[i].compsize_bof = len(bofobj.data[i])
        else:
          sample.attribute_props[i].shannon = self.calc_shannon(str(bofobj.data[i]))
          sample.attribute_props[i].rawsize = len(str(bofobj.data[i]))
          zlib_data = zlib.compress(str(bofobj.data[i]).encode(), 6)
          sample.attribute_props[i].compsize_zlib = len(zlib_data)
