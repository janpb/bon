#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Copyright 2016, 2017 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  \file analyzer.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description Analyzer for the bof data sets
#  \version 1.0.1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import os
import sys
import xml.etree.ElementTree as ET
import gzip
import shutil
from Bio import Phylo
from Bio.Phylo import BaseTree

from . import converter
from . import dataset
import database.database

sys.path.insert(1, os.path.join(sys.path[0], '../../../src'))
import bof

sys.path.insert(1, os.path.join(sys.path[0], '../../converter/lib'))
import fastq
import phylo
import bioxml

## Base class to analyze datasets
# It handles the database handling and shared functions.
# The analysis is a virtual function and needs its own implementation for each
# dataset
class DatasetAnalyzer:

  ## Constructor
  # Initalizes the required databse and BonConverter objects
  def __init__(self):
    self.write = False
    self.db = database.database.Database()
    self.dset = dataset.Dataset()
    self.name = 'noname_dset'
    self.attributes = {}
    self.bofconverter = converter.BofConverter(self.attributes)

  ## Helper method to get file size
  # @param fil, str, path to file
  def get_filesize(self, fil):
    return os.stat(fil).st_size

  ## Function to add a dataset into the analysis database
  # @param db database name
  def add_to_db(self, db):
    self.db.connect(db)
    dset_rid = self.db.insert_dset((self.dset.name, len(self.dset.samples)))
    for i in self.fmts:
      self.db.insert_fsize((i, self.dset.fsizes[i], dset_rid.lastrowid))

    samples = []
    sample_attribs = {}
    samples_total = 0
    for i in self.dset.samples:
      samples.append((i.name, str(i.id), i.size, dset_rid.lastrowid))
      for j in i.attribute_props:
        if i.id not in sample_attribs:
          sample_attribs[i.id] = []
        sample_attribs[i.id].append((j,
                                     i.attribute_props[j].rawsize,
                                     i.attribute_props[j].shannon,
                                     i.attribute_props[j].compsize_bof,
                                     i.attribute_props[j].compsize_zlib,
                                     dset_rid.lastrowid))
      samples_total += 1
      if len(samples) % 100000 == 0:
        self.db.insert_samples(samples, sample_attribs)
        print("\r\tDatasetAnalyzer::added {0} samples".format(samples_total),
              end='', file=sys.stderr)
        samples = []
        sample_attribs = {}
    if len(samples) > 0:
      self.db.insert_samples(samples, sample_attribs)
      print("\r\tDatasetAnalyzer::added {0} samples".format(samples_total),
              end='', file=sys.stderr)
      samples = []
      sample_attribs = {}
    self.db.update_attribsizes()
    print("\n", file=sys.stderr)

  ## Virtual method to analyze the dataset
  def analyze(self):
    raise NotImplementedError("analyze() needs an implementation")

  ## Helper method to get zipped file size
  # @param fname, str, path to file
  # @return compressed file size
  # The function compresses a text file uisng zlib  and reports its size
  def get_gzip_filsize(self, fname):
    fname_gzip = fname + '.gz'
    fh_gzip = gzip.open(fname_gzip, 'wb', compresslevel=6)
    fh = open(fname, 'rb')
    fh_gzip.write(fh.read())
    fh_gzip.close()
    fh.close()
    return os.stat(fname_gzip).st_size

## Phylogenentic tree analysis class
# Converts a NEXUS file, converts it to NeXML using biopython and converts it to
# BON, anlyzing its attributes. Biopython nexus to nexml conversion seems to
# loose information.
class PhyloTreeAnalyzer(DatasetAnalyzer):

  ## Constructor
  # Prepares the dataset analysis
  # @parameter name, str, name of dataset
  def __init__(self, name):
    super().__init__()
    self.fmts = ['nexml','cnexml', 'bof', 'cbof']
    self.dset.add_fmts(self.fmts)
    self.dset.name = name

  ## Analyze phylogenetic trees in NeXML and BON
  # Uses biopython to convert from NEXUS to NeXML
  # @parameter fil, str, path to NEXUS file
  def analyze(self, fil):
    print("Start: analyze dataset {0}".format(self.dset.name), file=sys.stderr)
    nexml = fil+".nexml"
    Phylo.convert(fil, "nexus", nexml, "nexml")
    self.dset.fsizes['nexml'] = self.get_filesize(nexml)
    self.dset.fsizes['cnexml'] = self.get_gzip_filsize(nexml)
    self.bofconverter.convert(phylo.PhyloTreeConverter(attributes=self.attributes),
                              self.dset,
                              nexml,
                              ['name', 'name', 'nodes'],
                              write=self.write)
    os.remove(nexml)
    print("\nEnd: analysis dataset {0}".format(self.dset.name), file=sys.stderr)

## NCBI's TinySeq XML analysis class
# Converts a NCBI TinySeq XML file to BON and analyses its attributes.
class TinySeqAnalyzer(DatasetAnalyzer):

  ## Constructor
  # Prepares the dataset analysis
  # @parameter name, str, name of dataset
  def __init__(self, name):
    super().__init__()
    self.fmts = ['xml', 'cxml', 'bof', 'cbof']
    self.dset.add_fmts(self.fmts)
    self.dset.name = name

  ## Analyze TinySeq XML files and corresponding BON files
  # @parameter fil, str, path to TinySeq XML file
  def analyze(self, fil):
    print("Start: analyze dataset {0}".format(self.dset.name), file=sys.stderr)
    self.dset.fsizes['xml'] = self.get_filesize(fil)
    self.dset.fsizes['cxml'] = self.get_gzip_filsize(fil)
    self.bofconverter.convert(bioxml.TinySeqConverter(attributes=self.attributes),
                              self.dset,
                              fil,
                              ['accver', 'gi', 'length'],
                              write=self.write)
    print("\nEnd: analysis dataset {0}".format(self.dset.name), file=sys.stderr)

## FASTQ analysis class
# Converts a FASTQ file to BON and analyses its attributes.
class FastqAnalyzer(DatasetAnalyzer):

  ## Constructor
  # Prepares the dataset analysis
  # @parameter name, str, name of dataset
  def __init__(self, name):
    super().__init__()
    self.fmts = ['fq', 'cfq', 'bof', 'cbof']
    self.dset.add_fmts(self.fmts)
    self.dset.name = name

  ## Analyze FASTQ XML files and corresponding BON files
  # @parameter fil, str, path to FASTQ file
  def analyze(self, fil):
    self.dset.fsizes['fq'] = self.get_filesize(fil)
    self.dset.fsizes['cfq'] = self.get_gzip_filsize(fil)
    print("Start: analyze dataset {0}".format(self.dset.name), file=sys.stderr)
    self.bofconverter.convert(fastq.FastqConverter(attributes=self.attributes),
                              self.dset,
                              fil,
                              ['spotid', 'readid', 'length'],
                              write=self.write)
    print("\nEnd: analysis dataset {0}".format(self.dset.name), file=sys.stderr)
