#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  \file summarizer.py
#  \copyright 2017 The University of Sydney
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description
#  \version 1.0.1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import database.database

class DatasetSummarizer:

  def __init__(self, db):
    self.db = database.database.Database()
    self.db.connect(db)
    self.compressed = {'cxml': 0, 'cnexml' : 0, 'cfq' : 0}

  def summarize_attributes(self):
    stmt = """CREATE TABLE attrib_summary AS SELECT a.dsetId AS dsetId,
                                                    d.name AS dset,
                                                    d.szDataDecomp AS szDataDecomp,
                                                    d.szDataCompBof AS szDataCompBof,
                                                    d.szDataCompZlib AS szDataCompZlib"""
    for i in self.db.execute_sql_cmd("""SELECT DISTINCT(name) FROM attribs"""):
      stmt += ", ROUND(SUM(CASE WHEN a.name='{0}' THEN a.h END) / COUNT(DISTINCT(a.sampleId))*1.0, 2) AS h{1}Avg\n".format(i[0], i[0].title())
    stmt += " FROM attribs a JOIN dsets d ON a.dsetId=d.id GROUP BY a.dsetId"
    self.db.execute_sql_cmd(stmt)

  def summarize_samples(self):
    stmt = """CREATE TEMP TABLE samples_summary AS
                               SELECT dsetId,
                               MIN(size) AS MinSize,
                               MAX(size) AS MaxSize,
                               AVG(size) AS AvgSize,
                               SUM(size) AS TotSize
                               FROM samples
                               GROUP BY dsetId"""
    self.db.execute_sql_cmd(stmt)

  def summarize_filesizes(self):
    formats = []
    for i in self.db.execute_sql_cmd("""SELECT DISTINCT(fmt) FROM fsizes"""):
      formats.append(i[0])
    stmt = "CREATE TABLE fsize_summary AS SELECT fs.dsetId AS dsetId"
    for i in formats:
      stmt +=  """,\nMAX(CASE WHEN fs.fmt='{0}' THEN fs.fsize ELSE 0 END) AS {0}""".format(i)
      if i == 'cbof':
        stmt += """,\nMAX(CASE WHEN fs.fmt='{0}' THEN ROUND(ats.szDataCompBof/fs.fsize, 2) ELSE 0 END) AS {0}_dataratio""".format(i)
      elif i in self.compressed:
        stmt += """,\nMAX(CASE WHEN fs.fmt='{0}' THEN ROUND(ats.szDataCompZlib/fs.fsize, 2) ELSE 0 END) AS {0}_dataratio""".format(i)
      else:
        stmt += """,\nMAX(CASE WHEN fs.fmt='{0}' THEN ROUND(ats.szDataDecomp/fs.fsize, 2) ELSE 0 END) AS {0}_dataratio""".format(i)
    stmt += """\n FROM fsizes fs
                JOIN attrib_summary ats ON
                fs.dsetId = ats.dsetId
                GROUP BY fs.dsetId"""
    self.db.execute_sql_cmd(stmt)

  def summarize(self):
    self.summarize_attributes()
    self.summarize_samples()
    self.summarize_filesizes()
    stmt = """CREATE TEMP TABLE tmp_summary AS
              SELECT d.name AS dset,
                     d.size AS dsetSize,
                     ats.szDataDecomp AS dataDecomp,
                     ats.szDataCompBof,
                     ats.szDataCompZlib,
                     fs.*,
                     ss.MinSize AS Minsize,
                     ss.MaxSize AS Maxsize,
                     ss.AvgSize AS Avgsize,
                     ss.TotSize AS Totsize
              FROM dsets d
              JOIN attrib_summary ats ON d.id=ats.dsetId
              JOIN fsize_summary fs ON fs.dsetId=d.id
              JOIN samples_summary ss ON ss.dsetId=d.id
              GROUP BY d.id"""
    self.db.execute_sql_cmd(stmt)
    fh_tbl = open('bof.fsizes.csv', 'w')
    header = ','.join(str(x[1]) for x in self.db.execute_sql_cmd("""PRAGMA table_info (tmp_summary)""")) + "\n"
    fh_tbl.write(header)
    for i in self.db.execute_sql_cmd("""SELECT * FROM tmp_summary"""):
      fh_tbl.write(",".join(str(x) for x in i) + "\n")
    fh_tbl.close()

    fh_tbl = open('bof.attribs.csv', 'w')
    header = ','.join(str(x[1]) for x in self.db.execute_sql_cmd("""PRAGMA table_info (attrib_summary)""")) + "\n"
    fh_tbl.write(header)
    for i in self.db.execute_sql_cmd("""SELECT * FROM attrib_summary"""):
      fh_tbl.write(",".join(str(x) for x in i) + "\n")
    fh_tbl.close()
