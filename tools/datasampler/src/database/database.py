#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  \file database.py
#  \copyright 2017,2018 The University of Sydney
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description
#  \version: 1.0.1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
import sqlite3

class Database:

  def __init__(self):
    self.db = None
    self.conn = None
    self.cur = None

  def connect(self, db):
    self.db = db
    self.conn = sqlite3.connect(self.db)
    self.execute_sql_cmd("PRAGMA foreign_keys = ON")
    self.prepare_tables()

  def prepare_tables(self):
    self.execute_sql_cmd("""CREATE TABLE IF NOT EXISTS dsets (
                            id             INTEGER PRIMARY KEY,
                            name           TEXT,
                            size           INT,
                            szDataDecomp   FLOAT,
                            szDataCompBof  FLOAT,
                            szDataCompZlib FLOAT,
                            UNIQUE(name, size))""")

    self.execute_sql_cmd("""CREATE TABLE IF NOT EXISTS fsizes (
                            id     INTEGER PRIMARY KEY,
                            fmt    TEXT,
                            fsize  FLOAT,
                            dsetId INT REFERENCES dsets,
                            UNIQUE(dsetId, fmt))""")

    self.execute_sql_cmd("""CREATE TABLE IF NOT EXISTS samples (
                            id       INTEGER PRIMARY KEY,
                            accs     TEXT,
                            sampleId TEXT,
                            size     INT,
                            dsetId   INT REFERENCES dsets,
                            UNIQUE(accs, id))""")

    self.execute_sql_cmd("""CREATE TABLE IF NOT EXISTS attribs (
                            id             INTEGER PRIMARY KEY,
                            name           TEXT,
                            szDataDecomp   FLOAT,
                            szDataCompBof  FLOAT,
                            szDataCompZlib FLOAT,
                            h              FLOAT,
                            dsetId         INT REFERENCES dsets,
                            sampleId       INT REFERENCES samples,
                            UNIQUE(sampleId, name))""")

  def execute_sql_cmd(self, cmd, values=()):
    #print(cmd, values)
    cur = self.conn.cursor()
    if len(values) == 0:
      cur.execute(cmd)
    else:
      cur.execute(cmd, values)
    self.conn.commit()
    return cur

  def execute_sql_manycmd(self, cmd, values=[]):
    #print(cmd, values)
    cur = self.conn.cursor()
    cur.executemany(cmd, values)
    self.conn.commit()
    return cur

  def insert_samples(self, samples, sample_attribs, phylo=False):
    self.execute_sql_cmd("""DROP TABLE IF EXISTS tmp_samples""")
    self.execute_sql_cmd("""CREATE TEMP TABLE tmp_samples (
                            accs TEXT, sampleId TEXT) """)

    self.execute_sql_manycmd("""INSERT OR ABORT INTO tmp_samples
                             (accs, sampleId) VALUES (?,?) """,
                             [(x[0], x[1]) for x in samples])

    self.execute_sql_manycmd("""INSERT OR ABORT INTO samples
                             (accs, sampleId, size, dsetId) VALUES (?,?,?,?)""",
                             samples)
    attribs = []
    for i in self.execute_sql_cmd("""SELECT s.sampleId, s.id
                                     FROM samples s, tmp_samples t
                                     WHERE t.accs=s.accs AND
                                           t.sampleId=s.sampleId"""):
      if i[0] in sample_attribs:
        for j in range(len(sample_attribs[i[0]])):
          sample_attribs[i[0]][j] += (i[1], )
          attribs.append(sample_attribs[i[0]][j])
    self.execute_sql_manycmd("""INSERT OR ABORT INTO attribs
                               (name, szDataDecomp, h, szDataCompBof,
                                szDataCompZlib, dsetId, sampleId)
                                VALUES (?,?,?,?,?,?,?)""", attribs)
    attribs = []

  def update_attribsizes(self):
    self.execute_sql_cmd("""UPDATE dsets SET
    szDataDecomp=(SELECT SUM(at.szDataDecomp)
                  FROM attribs at
                  WHERE at.dsetId=dsets.id),
    szDataCompBof=(SELECT SUM(CASE WHEN at.szDataCompBof > 0
                                THEN at.szDataCompBof
                                ELSE at.szDataDecomp
                           END)
                FROM attribs at
                WHERE at.dsetId=dsets.id),
    szDataCompZlib=(SELECT SUM(at.szDataCompZlib)
                          FROM attribs at
                          WHERE at.dsetId=dsets.id)
                          """)

  def insert_dset(self, dset):
    #print(dset)
    return self.execute_sql_cmd("""INSERT OR ABORT INTO dsets
                                   (name, size) VALUES (?,?)""", dset)

  def insert_fsize(self, fsize):
    #print(dset)
    return self.execute_sql_cmd("""INSERT OR ABORT INTO fsizes
                                   (fmt, fsize, dsetId) VALUES (?,?,?)""", fsize)
