# BON is the Biological Object Notation
*it's just that good*

## Abstract
The large size and high complexity of biological data can represent a major
methodological challenge for the analysis and exchange of data sets between
computers and applications. There has also been a substantial increase in the
amount of metadata associated with biological data sets, which is being
increasingly incorporated into existing data formats. Despite the existence
of structured formats based on XML, biological data sets are mainly formatted
using unstructured file formats, and the incorporation of metadata results in
increasingly complex parsing routines such that they become more error prone. To
overcome these problems, we present the “biological notation” (BON) format, a
new way to exchange and parse nearly all biological data sets more efficiently
and with less error than other currently available formats. Based on JavaScript
Object Notation (JSON), BON simplifies parsing by clearly separating the
biological data from its metadata and reduces complexity compared to XML based
formats. The ability to selectively compress data up to 87% compared to other
file formats and the reduced complexity results in improved transfer times and
less error prone applications.

Publication: https://doi.org/10.1038/s41598-018-28016-6
## Install

bon is implemented as a library and not a stand-alone tool in Python 3.6. It
uses only Python standard libraries. The bon implementation is in 'src/'.
The other directories contain demo tools and documentation.


### Simple install

 0. 'git clone https://gitlab.com/janpb/bon.git'

### Use as library via git submodule:

 0. `git submodule add https://gitlab.com/janpb/bon.git path/to/library`
 0. `git submodule update --init --recursive`


We wrote a simple bon viewer. Try:

`/tools/bofviewer/src/bellevue.py -h`

### Examples

- #### Convert a FASTQ file to bon and parse its content:

    `tools/converter/fastq2bof.py < examples/ERR1662731.subset.fastq | tools/bofviewer/src/bellevue.py`

- #### Convert a NCBI TinySeq XML file to bon and compress the sequence:

    `tools/converter/xml2bof.py -c sequence  <  examples/AP_000196.tinyseq.xml`
