#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  \file bon.py
#  \version 1.1.0
#  \author Jan P Buchmann    <lejosh@members.fsf.org>
#  \author Mathieu Fourment <mathieu.fourment@uts.edu.au>#
#  \acknowledgements Hans Buchmann <hans.buchmann@fhnw.ch>
#                    for suggesting base64
#  \description  The basic bon class. It handles adding, compression and
#                decompression of bon objects. It is The base class for bof
#                blocks and contains two nested classes, Header and
#                Compressor. This class has a putative virtual function to
#                accommodate conversion into different file formats.
# \bug Doesn't validate if data is already compressed or not.
#-------------------------------------------------------------------------------
import io
import sys
import json
import zlib
import base64

import bon_object
class Bon:

  ## Bon Header
  # Two mandatory attributes, size and data. Requires a dictionary for
  # all expected attributes within a bon object which value is either 0
  # (not compressed) or 1 (compressed). The putative virtual function parse
  # allows implementing specific parsing routines.
  # Example:
  #   data = {
  #           'sequence' : 1,
  #           'name' : 0
  #          }
  # This indicates two attributes per bon object, sequence and name. sequence
  # is compressed, name is not compressed.
  class Header:
    ## Constructor
    # @param attribs, dictionary, dictionary with bon attributes and if they are
    # compressed.
    def __init__(self, attribs={}):
      self.size = 0
      self.data = attribs

    # Function to export header for writing
    def export(self):
      return {'size' : self.size, 'data' : {x:self.data[x] for x in self.data}}

    # Function to parse minimum required header data
    def parse(self, headerobj):
      header = json.loads(headerobj)
      self.size = header.pop('size')
      self.data = header.pop('data')

  ## Bon compressor
  # This class handles the compression of data within bon objects using zlib
  # and base64. Data is compressed using zlib and written as an encoded base64
  # string. Compressing a bon object describes the compression of a specific
  # attribute within a bon object.
  class Compressor:
    ## Constructor
    # Uses a default compression level of 6 (see man zlib)
    def __init__(self):
      self.level = 6

    ## Function to compress data
    #@param data, string, data to compress
    def compress(self, data, lvl=0):
      if lvl < 1:
        lvl = self.level
      return base64.b64encode(zlib.compress(data.encode(), lvl)).decode()

    ## Function to decompress data
    #@param data, base64 string, data to decompress in base64 encoding
    def expand(self, data):
      return zlib.decompress(base64.b64decode(data)).decode()

  ## Constructor
  # Initailies a new Bon block with header and compressor.
  # @param data, dictionary. dictionary with bon attributes and if they are
  # compressed (see Header).
  def __init__(self, data={}):
    self.header = self.Header(data)
    self.compressor = self.Compressor()
    self.payload = []
    self.step_size = 10000
    self.sep = (',', ':')

  ## Helper function to compress whole payload
  def compress(self, attributes={}):
    if len(attributes) == 0:
      for i in self.header.data:
        self.header.data[i] = 1
    for i in self.payload:
      self.compress_single(i)

  ## Helper function to compress attributes in single bon object
  # @param bo, bon object instance
  def compress_single(self, bo):
    for i in bo.data:
      if i in self.header.data and self.header.data[i] == 1:
        bo.data[i] = self.compress_data(str(bo.data[i]))

  ## Helper function to compress data
  # @param data, str, data to compress
  # @return compressed data as base64 string
  def compress_data(self, data):
    return self.compressor.compress(data)

  ## Helper function to decompress whole payload
  def expand(self):
    for i in self.payload:
      self.expand_single(i)
    for i in self.header.data:
      self.header.data[i] = 0
  ## Helper function to decompress attributes in single bon object
  # @param bo, Bon_object() instance
  def expand_single(self, bo):
    for i in bo.data:
      if i in self.header.data and self.header.data[i] == 1:
        bo.data[i] = self.expand_data(bo.data[i])

  ## Function to check is an attribute is compressed
  #@input attribute_name, str, name of attribute to check
  #@return bool
  def isCompressed(self, attrib_name):
    if attrib_name in self.header.data and self.header.data[attrib_name] == 1:
      return True
    return False

  ## Helper function to decompress data
  # @param data, str, data as base64 strng
  # @return data as text string
  def expand_data(self, data):
    return self.compressor.expand(data)

  ## Function to add a bon object to the payload
  # It checks if the attributes are present in the header and for empty bon
  # objects.
  # @param bo, Bon_object()
  def add(self, bo):
    for i in bo.data:
      if i not in self.header.data:
        print("Warning: Unknown data attribute {0}. Skipping.".format(i), file=sys.stderr)
      else:
        if  self.header.data[i] == 1:
          bo.data[i] = self.compress_data(str(bo.data[i]))
    if len(bo.data) == 0:
      print("Warning: BO without attributes. Not adding to payload.", file=sys.stderr)
    else:
      self.payload.append(bo)
      self.header.size += 1
      if self.header.size % self.step_size == 0:
        print("\rbof::objects: added {0}".format(self.header.size), end='',
              file=sys.stderr)

  ## Function to export a subset as individual bon block
  # @param, list, payload list indices
  # @return a new instance of a Bon() with the subset as payload.
  def export_subset(self, entry_idx):
    subset = Bof()
    subset.header = self.header
    subset.payload = [self.payload[x] for x in entry_idx]
    subset.header.size = len(subset.payload)
    return subset

  ## Function to export the bon block as JSON to standard output.
  # Is can return a simple pretty version of the bon block
  #@param pretty_print, bool, use simple pretty print
  def export(self, pretty_print=False):
    indent = None
    objsep = ','
    if pretty_print == True:
      indent = 1
      objsep = ',\n'
    obj_count = 0
    print(json.dumps(self.header.export(), indent=indent, separators=self.sep),
          end='[')
    for i in self.payload:
      obj_count += 1
      if obj_count == self.header.size:
        objsep = ']'
      print(json.dumps(i.export(), indent=indent, separators=self.sep),
            end=objsep)
    print("\nbof::objects: {0} exported ".format(obj_count), file=sys.stderr)

  ## Function to write bon block as JSON to standard output stream.
  # Write the bon block as tight as possible to standard output using yield.
  def write(self):
    obj_count = 0
    end = ','
    for i in self.payload:
      stream = io.StringIO()
      if obj_count == 0:
        print(json.dumps(self.header.export(), separators=self.sep), end='[',
              file=stream)
      if obj_count == self.header.size - 1:
        end = ']'
      print(json.dumps(i.export(), separators=self.sep), end=end, file=stream)
      obj_count += 1
      yield stream.getvalue()

  def convert(self):
    raise NotImplementedError("Need implementation to convert")

  def parse(self):
    raise NotImplementedError("Need parsing implementation")
