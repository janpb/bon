#-------------------------------------------------------------------------------
#  Copyright 2016-2018 The University of Sydney
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  \file reader.py, which is part of the Bon format library
#  \author Jan P Buchmann <lejosh@members.fsf.org>
#  \author Mathieu Fourment <mathieu.fourment@uts.edu.au>
#  \version 1.1.0
#  \description A simple class to read bon streams from standard input
#               Since Python is lacking switch statements in its standard
#               library,  the code is based on a rather ugly
#               while/if/elif/else approach. In addition, reading one char
#               at the time could be maybe improved to read blocks.
#
#               A bon class is required as argument when reading, e.g. the Bon()
#               class or a derived and adjusted class thereof.
#-------------------------------------------------------------------------------
import sys

class BonReader:

  ## Constructor
  # Initializes a BonReader() and resets the block count
  def __init__(self):
    self.block_count = 0

  ## The read function to parse bon streams or files
  # @input parser, Bon() instance
  # @input src, str, bon file. If none is given, assumes standard input
  def read(self, parser, src=None):
    stream = sys.stdin
    if src != None:
      stream = open(src, 'r')
    level = 0
    obj = ''
    status = 0
    char = stream.read(1)
    while True:
      if len(char) == 0:
        print(">>End of stream", file=sys.stderr)
        break
      while status == 0:
        if char == '{':
          obj = char
          level += 1
          status = 1
          continue
        else:
          sys.exit("Invalid bof. Missing header. Abort")

      while status == 1:
        char = stream.read(1)
        if char == '"':
          status = 2
          obj += char
        elif char == '{':
          obj += char
          level += 1
        elif char == '}':
          obj += char
          level -= 1
          status = 3
        else:
          obj += char
        continue

      while status == 2: # string
        char = stream.read(1)
        if char == '\\':
          obj += char
          status = 4
        elif char == '"':
          obj += char
          status = 1
        else:
          obj += char

      while status == 3: # check if not nested object
        if level == 0:
          status = 5
        else:
          status = 1

      while status == 4: # escaped chacaters
        if char == 'n':
          stream.read(1)
        #elif char == '"':
          #obj += char
        #elif char == '/':
          #obj += char
        #elif char == '\\':
          #obj += char
        else:
          obj += stream.read(1)
        status = 2

      while status == 5: # check if header or object
        if char == '[': # header
          print(">>Start of archive", file=sys.stderr)
          #print("Header:", obj)
          parser.header.parse(obj)
          char = stream.read(1)
          obj = ''
          status = 0
        elif char == '{':
          #print("Object: ", obj)
          parser.parse(obj)
          obj = char
          level += 1
          status = 1
        elif char == ']':
          self.block_count += 1
          #print("Last: ", obj)
          parser.parse(obj)
          print(">>End of block {0}".format(self.block_count), file=sys.stderr)
          char = stream.read(1)
          status = 0
        else:
          char = stream.read(1)
