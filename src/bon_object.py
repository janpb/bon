#-------------------------------------------------------------------------------
#  \file bon_object.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \author Mathieu Fourment <mathieu.fourment@uts.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 1.0.1
#  \description  Base class for bon objects. The attributes are stored in a
#                dictionary mimicking a JSON structure. It handles adding and
#                exporting its attributes.
#-------------------------------------------------------------------------------

class BonObject:
  ## Constructor
  # Initializes a dictionary for the attributes
  def __init__(self):
    self.data = {}

  ## Helper function to add attributes.
  # Aborts upon adding duplicate attributes as this should not happen.
  def add_attribute(self, attrib_name, attrib_value):
    if attrib_name in self.data:
      sys.exit("Bof object:: {0} already exists. Abort".format(attrib_name))
    self.data[attrib_name] = attrib_value

  ## Helper function to get an attribute
  def get_attribute(self, attrib_name):
    return self.data[attrib_name]

  ## Helper function to export the attributes as dictionary,
  # mimicking a JSON object
  def export(self):
    return {x:self.data[x] for x in self.data}
